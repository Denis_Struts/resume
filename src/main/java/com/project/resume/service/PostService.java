package com.project.resume.service;

import com.project.resume.model.Post;
import com.project.resume.model.User;

public interface PostService {
    boolean addResume(Post post, User user);
}
