package com.project.resume.service;

import com.project.resume.model.User;

import java.util.Optional;

public interface UserService {

    Optional<User> findUserByUsername(String username);
    Optional<User> findUserByEmail(String email);
    Optional<User> findUserByPhoneNumber(String phoneNumber);
    boolean sign_up(User user);
}
