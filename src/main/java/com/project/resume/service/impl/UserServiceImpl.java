package com.project.resume.service.impl;

import com.project.resume.model.Roles;
import com.project.resume.model.User;
import com.project.resume.repository.RoleRepository;
import com.project.resume.repository.UserRepository;
import com.project.resume.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

    private UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           BCryptPasswordEncoder bCryptPasswordEncoder,
                           RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.roleRepository = roleRepository;
    }

    @Override
    public Optional<User> findUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Optional<User> findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Optional<User> findUserByPhoneNumber(String phoneNumber) { return userRepository.findByPhoneNumber(phoneNumber); }

    @Override
    public boolean sign_up(User user) {
        if (userRepository.findByUsername(user.getUsername()).isPresent()
        && userRepository.findByEmail(user.getEmail()).isPresent()) {
            return false;
        }

        user.setActive(true);
        user.setRoles(Collections.singletonList(roleRepository
                .findByRole(Roles.ROLE_USER.name())));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        userRepository.save(user);
        return true;
    }
}
