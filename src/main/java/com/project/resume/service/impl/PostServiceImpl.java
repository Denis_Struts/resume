package com.project.resume.service.impl;

import com.project.resume.model.Post;
import com.project.resume.model.User;
import com.project.resume.repository.PostRepository;
import com.project.resume.repository.UserRepository;
import com.project.resume.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean addResume(Post post, User user) {
        if (user.getPost() != null) {
            return false;
        }

        post.setUser(user);
        postRepository.save(post);
        user.setPost(post);
        userRepository.save(user);
        return true;
    }
}
