package com.project.resume.controller;

import com.project.resume.model.Post;
import com.project.resume.repository.PostRepository;
import com.project.resume.repository.UserRepository;
import com.project.resume.service.PostService;
import com.project.resume.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Optional;

import static com.project.resume.controller.ResumeController.headerView;


@Controller
public class PostController {

    private static Logger LOGGER = LogManager.getLogger(PostController.class);

    private PostService postService;
    private UserService userService;
    private UserRepository userRepository;
    private PostRepository postRepository;

    @Autowired
    public PostController(PostService postService, UserService userService,
                          UserRepository userRepository,
                          PostRepository postRepository) {
        this.postService = postService;
        this.userRepository = userRepository;
        this.userService = userService;
        this.postRepository = postRepository;
    }

    public static boolean doesHavePost(UserRepository userRepository, Principal principal) {
        if (principal != null
                && userRepository.findByUsername(principal.getName()).isPresent()
                && userRepository.findByUsername(principal.getName()).get().getPost()
                != null) {
            return true;
        } else {
            return false;
        }
    }

    @GetMapping("/newPost")
    public String newPost(Post post, Model model, Principal principal) {
        model = headerView(model, principal, userRepository);
        model.addAttribute("doesHavePost", doesHavePost(userRepository, principal));
        return "/newPost";
    }

    @PostMapping("/newPost")
    public String addPost(@Valid Post post, BindingResult bindingResult,
                          Model model, Principal principal) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("doesHavePost", doesHavePost(userRepository, principal));
            return "/newPost";
        }

        postService.addResume(post, userRepository
                .findByUsername(principal.getName()).get());

        if (userRepository.findByUsername(principal.getName())
                .get().getPost() != null ) {
            model.addAttribute("successMessage", "Post successfully created");
            model.addAttribute("doesHavePost", false);
        }
        return "/newPost";
    }

    @GetMapping("/post/{id}")
    public String getPostById(@PathVariable Long id, Principal principal,
                              Model model) {

        model = headerView(model, principal, userRepository);

        Optional<Post> optionalPost = postRepository.findById(id);

        if (optionalPost.isPresent()) {
            Post post = optionalPost.get();
            model.addAttribute("post", post);

            if (principal != null
                    && post.getUser().getUsername().equals(principal.getName())) {
                model.addAttribute("isAuthor", true);
            }

            return "/post";
        }

        return "redirect:error";

    }

    @PostMapping("/deletePost")
    public String deletePost(Principal principal, Model model) {
        if (principal != null
                && userRepository.findByUsername(principal.getName()).get().getPost()
                != null) {
            Post post = userRepository.findByUsername(principal.getName())
                    .get().getPost();
            userRepository.findByUsername(principal.getName()).get().setPost(null);
            postRepository.delete(post);
        }
        return "redirect:/index";
    }
}
