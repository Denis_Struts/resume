package com.project.resume.controller;

import com.project.resume.model.Post;
import com.project.resume.repository.PostRepository;
import com.project.resume.repository.UserRepository;
import com.project.resume.util.Pager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Map;

@Controller
public class ResumeController {

    private static Logger LOGGER = LogManager.getLogger(ResumeController.class);

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/index")
    public String greeting(@RequestParam(defaultValue = "1") int page, Principal principal, Model model) {
        model = headerView(model, principal, userRepository);

        Pageable pageable = PageRequest.of(page - 1, 7, Sort.Direction.DESC, "createDate");
        Page<Post> posts = postRepository.findAllByOrderByCreateDateDesc(pageable);

        model.addAttribute("posts", posts);
        model.addAttribute("pager", new Pager(posts));
        return "/index";
    }

    @GetMapping("/")
    public String main(Map<String, Object> model) {
        return "redirect:/index";
    }

    public static Model headerView(Model model, Principal principal, UserRepository userRepository) {
        if (PostController.doesHavePost(userRepository, principal)) {
            model.addAttribute("number", userRepository.findByUsername(principal.getName()).get().getPost().getId());
            model.addAttribute("doesHavePost", true);
        } else {
            model.addAttribute("doesHavePost", false);
        }

        return model;
    }

    @GetMapping("/search")
    public String searchField(@RequestParam String search,
                              @RequestParam(defaultValue = "1") int page, Principal principal, Model model) {
        model = headerView(model, principal, userRepository);

        Pageable pageable = PageRequest.of(page - 1, 7, Sort.Direction.DESC, "createDate");

        Page<Post> posts;

        if (search.isEmpty()) {
            posts = postRepository.findAllByOrderByCreateDateDesc(pageable);
        } else {
            posts = postRepository.findAllByPosition(pageable, search);
        }

        model.addAttribute("posts", posts);
        model.addAttribute("pager", new Pager(posts));
        return "/index";
    }

    @GetMapping("/error")
    public String error(Model model, Principal principal) {
        model = headerView(model, principal, userRepository);
        return "/error";
    }
}
