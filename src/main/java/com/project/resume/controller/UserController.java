package com.project.resume.controller;

import com.project.resume.model.User;
import javax.validation.Valid;

import com.project.resume.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @GetMapping("/sign-up")
    public String sign_up(User user, Model model, Principal principal) {
        return "sign-up";
    }

    @PostMapping("/sign-up")
    public String addUser(@Valid User user, BindingResult bindingResult,
                          Model model) {
        if (userService.findUserByUsername(user.getUsername()).isPresent()) {
            bindingResult.rejectValue("username", "error.user",
                    "* User with provided username exists already");
            return "sign-up";
        }

        if (userService.findUserByEmail(user.getEmail()).isPresent()) {
            bindingResult.rejectValue("email", "error.user",
                    "* User with provided email exists already");
            return "sign-up";
        }

        if (userService.findUserByPhoneNumber(user.getPhoneNumber()).isPresent()) {
            bindingResult.rejectValue("phoneNumber", "error.user",
                    "* User with provided phone number exits already");
            return "sign-up";
        }

        if (bindingResult.hasErrors()) {
            return "sign-up";
        } else {
            userService.sign_up(user);

            if (userService.findUserByUsername(user.getUsername()).isPresent()) {
                model.addAttribute("successMessage",
                        "User have been registered successfully");
                model.addAttribute(user);
            }
        }

        return "sign-up";
    }
}
