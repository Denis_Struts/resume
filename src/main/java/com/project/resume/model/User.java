package com.project.resume.model;

import javax.persistence.*;
import java.util.List;
import javax.validation.constraints.*;

@Table(name = "users")
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id",unique = true, nullable = false)
    private Integer id;

    @Column(name = "username")
    @NotBlank(message = "* Provide your username")
    private String username;

    @Column(name = "password")
    @NotBlank(message = "* Provide your password")
    private String password;

    @Column(name = "phone_number")
    private String phoneNumber; //todo to camelcase

    @Column(name = "email")
    @Email(message = "* Invalid email address format")
    private String email;

    @Column(name = "first_name")
    @NotBlank(message = "* Provide your first name")
    private String firstName; //todo to camelcase

    @Column(name = "middle_name")
    @NotBlank(message = "* Provide your middle name")
    private String middleName; //todo to camelcase

    @Column(name = "last_name")
    @NotBlank(message = "* Provide your last name")
    private String lastName; //todo to camelcase

    @Column(name = "age", nullable = false)
    @Min(value = 16)
    @NotNull(message = "* Provide your age")
    private Integer age;

    @Column(name = "active")
    private Boolean active;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "post_id")
    private Post post;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_role",
                joinColumns = @JoinColumn(name = "user_id"),
                inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Role> roles;

    public User() {

    }

    public User(String username, String password, String phoneNumber, String email) {
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
