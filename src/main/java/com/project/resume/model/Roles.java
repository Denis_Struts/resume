package com.project.resume.model;

public enum Roles {
    ROLE_USER,
    ROLE_ADMIN;
}
