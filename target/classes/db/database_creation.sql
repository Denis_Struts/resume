use test_db;

create table users
(
    id           int auto_increment
        primary key,
    username     varchar(45) not null,
    password     varchar(45) not null,
    phone_number varchar(45) not null,
    email        varchar(45) not null,
    post_id      int         null,
    first_name   varchar(20) not null,
    middle_name  varchar(20) not null,
    last_name    varchar(20) not null,
    age          int         not null,
    constraint email
        unique (email),
    constraint phone_number
        unique (phone_number),
    constraint username
        unique (username),
    constraint FK5na1mqr6obdc53ool8egadvar
        foreign key (post_id) references posts (id)
);
    
create table posts
(
    id              int auto_increment
        primary key,
    work_experience varchar(1500) not null,
    education       varchar(1000) not null,
    about_me        varchar(1000) not null
);

create table roles
(
    id   int         not null
        primary key,
    role varchar(32) not null,
    constraint roles_role_uindex
        unique (role)
);

create table user_role
(
    user_id int not null,
    role_id int not null,
    constraint user_role_roles_id_fk
        foreign key (role_id) references roles (id),
    constraint user_role_users_id_fk
        foreign key (user_id) references users (id)
);


